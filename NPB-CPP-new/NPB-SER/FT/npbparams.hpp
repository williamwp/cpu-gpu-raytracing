/* CLASS = C */
/*
c  This file is generated automatically by the setparams utility.
c  It sets the number of processors and the class_npb of the NPB
c  in this directory. Do not modify it by hand.
*/
#define	NX	512
#define	NY	512
#define	NZ	512
#define	MAXDIM	512
#define	NITER_DEFAULT	20
#define	NTOTAL	134217728
#define	CONVERTDOUBLE	FALSE
#define COMPILETIME "12 Mar 2022"
#define NPBVERSION "4.0"
#define CS1 "g++ -std=c++14"
#define CS2 "$(CC)"
#define CS3 "-lm "
#define CS4 "-I../common "
#define CS5 "-O3"
#define CS6 "-O3"
#define CS7 "randdp"
