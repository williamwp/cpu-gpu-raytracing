#define CLASS 'S'
/*
   This file is generated automatically by the setparams utility.
   It sets the number of processors and the class of the NPB
   in this directory. Do not modify it by hand.   */
   
#define COMPILETIME "12 Mar 2022"
#define NPBVERSION "4.0"
#define CC "g++ -std=c++14"
#define CFLAGS "-O3"
#define CLINK "$(CC)"
#define CLINKFLAGS "-O3"
#define C_LIB "-lm -ltbb"
#define C_INC "-I../common "
