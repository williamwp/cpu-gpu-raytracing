#include <assert.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>

/* every tool needs to include this once */
#include "nvbit_tool.h"

/* nvbit interface file */
#include "nvbit.h"

/* nvbit utility functions */
#include "utils/utils.h"

/* kernel id counter, maintained in system memory */
uint32_t kernel_id = 0;

#define LINE_SIZE_BYTES 128

static unsigned cacheSize = 16 * 1024 * 1024;
static unsigned lineSize = LINE_SIZE_BYTES;
static unsigned assoc = 16;

static unsigned hitCount = 0;
static unsigned missCount = 0;
static unsigned accessCount = 0;

static unsigned setCount = 0;
static unsigned lineCount = 0;
__device__ static void **tag_array = 0;

__device__ static unsigned *lru = 0;

__device__ static void cache_sim_read(void *addr) {
unsigned idx = ((unsigned long long)addr / lineSize) % setCount;
void **set = &tag_array[idx * assoc];
unsigned lru_idx = lru[idx];
unsigned hit = 0;
for (unsigned i = 0; i < assoc; i++) {
if (set[i] == addr) {
hit = 1;
lru[idx] = i;
break;
}
}
if (hit) {
hitCount++;
} else {
missCount++;
set[lru_idx] = addr;
lru[idx] = lru_idx + 1;
lru[idx] = (lru[idx] == assoc) ? 0 : lru[idx];
}
accessCount++;
}

__device__ static void cache_sim_write(void *addr) {
cache_sim_read(addr);
}





/* global control variables for this tool */
uint32_t instr_begin_interval = 0;
uint32_t instr_end_interval = UINT32_MAX;
uint32_t ker_begin_interval = 0;
uint32_t ker_end_interval = UINT32_MAX;
int verbose = 0;
int count_warp_level = 1;
int exclude_pred_off = 0;

/* a pthread mutex, used to prevent multiple kernels to run concurrently and
 * therefore to "corrupt" the counter variable */
pthread_mutex_t mutex;


/* nvbit_at_init() is executed as soon as the nvbit tool is loaded. We typically
 * do initializations in this call. In this case for instance we get some
 * environment variables values which we use as input arguments to the tool */
void nvbit_at_init() {
    /* just make sure all managed variables are allocated on GPU */
    setenv("CUDA_MANAGED_FORCE_DEVICE_ALLOC", "1", 1);

    /* we get some environment variables that are going to be use to selectively
     * instrument (within a interval of kernel indexes and instructions). By
     * default we instrument everything. */
    GET_VAR_INT(
        instr_begin_interval, "INSTR_BEGIN", 0,
        "Beginning of the instruction interval where to apply instrumentation");
    GET_VAR_INT(
        instr_end_interval, "INSTR_END", UINT32_MAX,
        "End of the instruction interval where to apply instrumentation");
    GET_VAR_INT(ker_begin_interval, "KERNEL_BEGIN", 0,
                "Beginning of the kernel launch interval where to apply "
                "instrumentation");
    GET_VAR_INT(
        ker_end_interval, "KERNEL_END", UINT32_MAX,
        "End of the kernel launch interval where to apply instrumentation");
    GET_VAR_INT(count_warp_level, "COUNT_WARP_LEVEL", 1,
                "Count warp level or thread level instructions");
    GET_VAR_INT(exclude_pred_off, "EXCLUDE_PRED_OFF", 0,
                "Exclude predicated off instruction from count");
    GET_VAR_INT(verbose, "TOOL_VERBOSE", 0, "Enable verbosity inside the tool");
    std::string pad(100, '-');
    printf("%s\n", pad.c_str());

setCount = cacheSize / (assoc * lineSize);
tag_array = (void**) malloc(sizeof(void*) * setCount * assoc);
lru = (unsigned*) malloc(sizeof(unsigned) * setCount);
for (unsigned i = 0; i < setCount; i++) {
lru[i] = 0;
for(unsigned j=0;j < assoc;j++)
{
tag_array[(i * assoc) + j] = 0;
}
}


}

void nvbit_at_cuda_event(CUcontext ctx, int is_exit, nvbit_api_cuda_t cbid,
                         const char *API_name, void *params, CUresult *pStatus) {
   
    //print the APIs executed by the rendering applications.
//    printf("%s\n", API_name);
    
    //start to count the number of each API
 //   int API_name_count = 0;
 //   const char *a = API_name;
 //   if(strcmp(a, a) == 0)
 //   {
 //       API_name_count++;
 //   }
   
}

void nvbit_at_term() {
//    printf("\n\nEND OF APPLICATION\n");

float miss_rate = ((float)missCount) / ((float)accessCount);
float hit_rate = ((float)hitCount) / ((float)accessCount);
printf("Cache Size (Bytes): %d\n", cacheSize);
printf("Line Size (Bytes): %d\n", lineSize);
printf("Associativity: %d\n", assoc);
printf("Total Sets: %d\n", setCount);
printf("Total Lines: %d\n", setCount * assoc);
printf("Access Count: %d\n", accessCount);
printf("Hit Count: %d (%f)\n", hitCount, hit_rate);
printf("Miss Count: %d (%f)\n", missCount, miss_rate);
free(tag_array);
free(lru);

}

/*
static __device__ void cache_sim_callback(int offset, int size, int type, int kind,void *arg0, void *arg1)
{
if(type == LOAD_OP || type == ATOMIC_OP || type == CVT_OP ){
cache_sim_read(arg0);
} else if (type == STORE_OP) {
cache_sim_write(arg0);
}
}
*/

__global__ void empty_kernel() {}

void nvbit_atrace_callback(NVBIT_ATRACE_ACTION action, const nvbit_kernel_info_t *kernel, const nvbit_atrace_t *atrace){
if (action == NVBIT_ATRACE_CREATED) {
               nvbit_enable_instrumented_kernel(empty_kernel, 0);              }

//void nvbit_init(){
//    nvbit_at_init();
  //  nvbit_register_atrace_callback(nvbit_atrace_callback);
  //  nvbit_register_callback(cache_sim_callback, (void *) 0); 
//}

  void nvbit_finalize() { nvbit_at_term(); }
}

