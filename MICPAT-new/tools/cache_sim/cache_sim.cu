#include "/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/core/nvbit.h"
#include <cstdio>
#include <cstdlib>

#define LINE_SIZE_BYTES 128

static unsigned cacheSize = 16 * 1024 * 1024;
static unsigned lineSize = LINE_SIZE_BYTES;
static unsigned assoc = 16;

static unsigned hitCount = 0;
static unsigned missCount = 0;
static unsigned accessCount = 0;

static unsigned setCount = 0;
static unsigned lineCount = 0;
__device__ static void **tag_array = 0;

__device__ static unsigned *lru = 0;

__device__ static void cache_sim_read(void *addr) {
unsigned idx = ((unsigned long long)addr / lineSize) % setCount;
void **set = &tag_array[idx * assoc];
unsigned lru_idx = lru[idx];
unsigned hit = 0;
for (unsigned i = 0; i < assoc; i++) {
if (set[i] == addr) {
hit = 1;
lru[idx] = i;
break;
}
}
if (hit) {
hitCount++;
} else {
missCount++;
set[lru_idx] = addr;
lru[idx] = lru_idx + 1;
lru[idx] = (lru[idx] == assoc) ? 0 : lru[idx];
}
accessCount++;
}
__device__ static void cache_sim_write(void *addr) {
cache_sim_read(addr);
}
void nvbit_at_init() {
setCount = cacheSize / (assoc * lineSize);
tag_array = (void**) malloc(sizeof(void*) * setCount * assoc);
lru = (unsigned*) malloc(sizeof(unsigned) * setCount);
for (unsigned i = 0; i < setCount; i++) {
lru[i] = 0;
for(unsigned j=0;j < assoc;j++)
{
tag_array[(i * assoc) + j] = 0;
}
}
}
void nvbit_at_term() {
float miss_rate = ((float)missCount) / ((float)accessCount);
float hit_rate = ((float)hitCount) / ((float)accessCount);
printf("Cache Size (Bytes): %d\n", cacheSize);
printf("Line Size (Bytes): %d\n", lineSize);
printf("Associativity: %d\n", assoc);
printf("Total Sets: %d\n", setCount);
printf("Total Lines: %d\n", setCount * assoc);
printf("Access Count: %d\n", accessCount);
printf("Hit Count: %d (%f)\n", hitCount, hit_rate);
printf("Miss Count: %d (%f)\n", missCount, miss_rate);
free(tag_array);
free(lru);
}


static __device__ void cache_sim_callback(int offset, int size, int type, int kind,void *arg0, void *arg1)
{
if(type == LOAD_OP || type == ATOMIC_OP || type == CVT_OP ){
cache_sim_read(arg0);
} else if (type == STORE_OP) {
cache_sim_write(arg0);
    }
}


/*
extern "C" {
__global__ void empty_kernel() {}
NVBIT_EXPORT void nvbit_atrace_callback(NVBIT_ATRACE_ACTION action, const nvbit_kernel_info_t *kernel, const nvbit_atrace_t *atrace){
if (action == NVBIT_ATRACE_CREATED) { 
               nvbit_enable_instrumented_kernel(empty_kernel, 0);	       } 
	     }
NVBIT_EXPORT void nvbit_init() { 
                nvbit_at_init(); 
		nvbit_register_atrace_callback(nvbit_atrace_callback); 
		nvbit_register_callback(cache_sim_callback, (void *) 0); }
NVBIT_EXPORT void nvbit_finalize() { nvbit_at_term(); }
}
*/
// extern "C"  
