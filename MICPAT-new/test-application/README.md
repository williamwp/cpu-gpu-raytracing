# Introduction
This is a raytraced rendering of the Schwarzchild Black Hole.

# Compiling
install the SDL1.2, freeglut and CUDA libraries to compile and run the following:

$ apt-get install libsdl1.2-dev libsdl-image1.2-dev freeglut3-dev
$ cd test-application  
$ make
$ ./bhfs 
