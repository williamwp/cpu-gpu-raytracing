#!/bin/bash

echo "Find the register reuse in the record-reg-vals.txt"  

# read i
# echo $i
# read m
# j=$(($m * 5))
# echo $j

# echo "Enter the register you want to find"   
# echo "The results of the register reuse number"
# grep -c reuse 186-all-record-reg-vals.txt
# echo "The results of the register R1.reuse number"
# grep -c R1.reuse 186-all-record-reg-vals.txt

# echo "The results of the register R2.reuse number"
# grep -c R2.reuse 186-all-record-reg-vals.txt

# echo "The results of the register R3.reuse number"
# grep -c R3.reuse 186-all-record-reg-vals.txt


#num=$(grep -c R4.reuse 186-all-record-reg-vals.txt)
# echo "The results of the register R4.reuse number = $num"

# -----------------------------------------------------------------------------------------
# divide the file into several files
# -----------------------------------------------------------------------------------------
if false; then
sed -n '1, 32768p' 186-all-record-reg-vals.txt > tmp1.txt;
sed -n "32769, 65536p" 186-all-record-reg-vals.txt > tmp2.txt;
sed -n "65537, 131072p" 186-all-record-reg-vals.txt > tmp3.txt;
sed -n "131073, 262144p" 186-all-record-reg-vals.txt > tmp4.txt;
sed -n "262145, 524288p" 186-all-record-reg-vals.txt > tmp5.txt;
sed -n "523289, 1048576p" 186-all-record-reg-vals.txt > tmp6.txt;
sed -n "1048577, 2097152p" 186-all-record-reg-vals.txt > tmp7.txt;
sed -n "2097153, 4194304p" 186-all-record-reg-vals.txt > tmp8.txt;
sed -n "4194305, 8388608p" 186-all-record-reg-vals.txt > tmp9.txt;
sed -n "8388609, 16777216p" 186-all-record-reg-vals.txt > tmp10.txt;
sed -n '16777217, $p' 186-all-record-reg-vals.txt > tmp11.txt;
fi

if false; then
sed -n "$((2**15)), 65536p" 186-all-record-reg-vals.txt > tmp3.txt
sed -n "$((2**16)), 131072p" 186-all-record-reg-vals.txt > tmp4.txt
sed -n "$((2**17)), 262144p" 186-all-record-reg-vals.txt > tmp5.txt
sed -n "$((2**18)), 524288p" 186-all-record-reg-vals.txt > tmp6.txt
sed -n "$((2**19)), 1048576p" 186-all-record-reg-vals.txt > tmp7.txt
sed -n "$((2**20)), 2097152p" 186-all-record-reg-vals.txt > tmp8.txt
sed -n "$((2**21)), 4194304p" 186-all-record-reg-vals.txt > tmp9.txt
sed -n "$((2**22)), 8388608p" 186-all-record-reg-vals.txt > tmp10.txt
sed -n "$((2**23)), 16777216p" 186-all-record-reg-vals.txt > tmp11.txt
sed -n "$((2**24)), $" 186-all-record-reg-vals.txt > tmp12.txt
fi

# -----------------------------------------------------------------------------------------
# main loop function 
# -----------------------------------------------------------------------------------------
#sed -n '1, 32768p' 186-all-record-reg-vals.txt

n=32

for(( i = 0; i < $n; i++ ));
do
    reg_num=$(grep -c R$i.reuse  186-all-record-reg-vals.txt )
    echo "The results of the register R$i.reuse number = $reg_num"
    let "reg_total+=$reg_num"
done
echo "Total register reuse number of reuse distance $k = $reg_total "
#    echo "-------------------------------------------------------------------------------"

# echo "average register reuse number = "
awk 'BEGIN{printf "Average register reuse number = %.2f\n",'$reg_total'/'$n'}'


# ----------------------------------------------------------------------------------------
# annotation
# ----------------------------------------------------------------------------------------
if false; then
m=70
for(( j = 0; j < $m; j++ ));
do
warp_num=$(grep -c "warp $j" 186-all-record-reg-vals.txt)
echo "The results of the warp $j  number = $warp_num";
let "warp_total+=$warp_num"
done
echo "Total warp number = $warp_total "
# echo "average warp number = "
awk 'BEGIN{printf "Average warp number = %.2f\n",'$warp_total'/'$m'}'

fi


