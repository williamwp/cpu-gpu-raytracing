#!/bin/bash

#sed -n "1, 32768p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n '1, 32768p' 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "32769, 65536p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "65537, 131072p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "131073, 262144p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "262145, 524288p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "523289, 1048576p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "1048577, 2097152p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "2097153, 4194304p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "4194305, 8388608p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n "8388609, 16777216p" 182-all-mem-printf.txt | grep -c  "reuse"
sed -n '16777217, $p' 182-all-mem-printf.txt | grep -c  "reuse"


if false; then
a=$(sed -n "1, 10099p" 182-all-mem-printf.txt)
grep -c  "reuse" $a
fi
# wrong code:"grep: unrecognized option '-------------'
# Usage: grep [OPTION]... PATTERN [FILE]...
# Try 'grep --help' for more information  "

