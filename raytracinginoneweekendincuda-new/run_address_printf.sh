#!/bin/bash

start_time=$(date +%s)
sleep 1

echo "Find the address reuse in the mem_printf.txt"  

# read i
# echo $i
# read m
# j=$(($m * 5))
# echo $j

# echo "Enter the register you want to find"   
# echo "The results of the register reuse number"
# grep -c reuse 182-all-mem-printf.txt
# echo "The results of the register R1.reuse number"
# grep -c R1.reuse 182-all-mem-printf.txt

# echo "The results of the register R2.reuse number"
# grep -c R2.reuse 182-all-mem-printf.txt

# echo "The results of the register R3.reuse number"
# grep -c R3.reuse 182-all-mem-printf.txt


#num=$(grep -c R4.reuse 182-all-mem-printf.txt)
# echo "The results of the register R4.reuse number = $num"

# -----------------------------------------------------------------------------------------
# divide the file into several files
# -----------------------------------------------------------------------------------------
sed -n '1, 32768p' 182-all-mem-printf.txt > tmp1.txt;
sed -n "32769, 65536p" 182-all-mem-printf.txt > tmp2.txt;
sed -n "65537, 131072p" 182-all-mem-printf.txt > tmp3.txt;
sed -n "131073, 262144p" 182-all-mem-printf.txt > tmp4.txt;
sed -n "262145, 524288p" 182-all-mem-printf.txt > tmp5.txt;
sed -n "523289, 1048576p" 182-all-mem-printf.txt > tmp6.txt;
sed -n "1048577, 2097152p" 182-all-mem-printf.txt > tmp7.txt;
sed -n "2097153, 4194304p" 182-all-mem-printf.txt > tmp8.txt;
sed -n "4194305, 8388608p" 182-all-mem-printf.txt > tmp9.txt;
sed -n "8388609, 16777216p" 182-all-mem-printf.txt > tmp10.txt;
sed -n '16777217, $p' 182-all-mem-printf.txt > tmp11.txt;

if false; then
sed -n "$((2**15)), 65536p" 182-all-mem-printf.txt > tmp3.txt
sed -n "$((2**16)), 131072p" 182-all-mem-printf.txt > tmp4.txt
sed -n "$((2**17)), 262144p" 182-all-mem-printf.txt > tmp5.txt
sed -n "$((2**18)), 524288p" 182-all-mem-printf.txt > tmp6.txt
sed -n "$((2**19)), 1048576p" 182-all-mem-printf.txt > tmp7.txt
sed -n "$((2**20)), 2097152p" 182-all-mem-printf.txt > tmp8.txt
sed -n "$((2**21)), 4194304p" 182-all-mem-printf.txt > tmp9.txt
sed -n "$((2**22)), 8388608p" 182-all-mem-printf.txt > tmp10.txt
sed -n "$((2**23)), 16777216p" 182-all-mem-printf.txt > tmp11.txt
sed -n '$((2**24)), $p' 182-all-mem-printf.txt > tmp12.txt
fi

# -----------------------------------------------------------------------------------------
# main loop function 
# -----------------------------------------------------------------------------------------
n=32
for(( k = 1; k < 12; k++ ));
do
    addr_num=$(grep -c "0x00007f16846" tmp$k.txt)
    echo "The reuse of the address area 0x00007f16846 for $k = $addr_num"
    let "addr_total+=$addr_num"
done

    echo "Total reuse number of address 0x00007f16846 = $addr_total "

#   echo "------------------------------------------"

# echo "average register reuse number = "
awk 'BEGIN{printf "Average reuse number of address 0x00007f16846 = %.2f\n",'$addr_total'/'$n'}'


# ---------------------------------------------------------------------------
# annotation
# ---------------------------------------------------------------------------
if false; then
m=70
for(( j = 0; j < $m; j++ ));
do
warp_num=$(grep -c "warp $j" 182-all-mem-printf.txt)
echo "The results of the warp $j  number = $warp_num";
let "warp_total+=$warp_num"
done
echo "Total warp number = $warp_total "
# echo "average warp number = "
awk 'BEGIN{printf "Average warp number = %.2f\n",'$warp_total'/'$m'}'

fi

end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "build kernel time is $(($cost_time/60)) min $(($cost_time%60)) s"
