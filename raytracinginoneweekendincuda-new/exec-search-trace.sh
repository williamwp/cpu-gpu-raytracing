#!/bin/bash

start_time=$(date +%s)
sleep 1
 
echo "Find the reuse numbers of address 0x0000 7f85 3eXX XXXX  in all-mem-trace.txt"

#sed -n "1, 32768p" 182-all-mem-trace.txt | grep -c  "7f853e"

aa1=$(sed -n '1, 32768p' 182-all-mem-trace.txt | grep -c  "7f853e")
echo "1#  The first interval 1 --> 32768 =  $aa1"
for(( i = 15; i < 24; i++ ));
do
j=$[$i+1]
k=$[$i-13]
# $((2**15)) is the begining part of the interval
aa2=$(sed -n "$((2**i)), $((2**j))p" 182-all-mem-trace.txt | grep -c  "7f853e")
echo "$k#  The middle interval from $((2**i)) to $((2**j)) = $aa2 "
done

aa3=$(sed -n '16777218, $p' 182-all-mem-trace.txt | grep -c  "7f853e")
echo "11#  The end interval 16777218 --> END = $aa3"


if false; then
a=$(sed -n "1, 10099p" 182-all-mem-trace.txt)
grep -c  "7f853e3" $a
fi
# wrong code:"grep: unrecognized option '-------------'
# Usage: grep [OPTION]... PATTERN [FILE]...
# Try 'grep --help' for more information  "

end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "build kernel time is $(($cost_time/60)) min $(($cost_time%60)) s"
