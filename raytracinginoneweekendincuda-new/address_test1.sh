#!/bin/bash

aa1=$(sed -n "1, 32768p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "1#  1 --> 32768 = $aa1"

aa2=$(sed -n "32769, 65536p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "2#  32769 --> 65536 =$aa2 "

aa3=$(sed -n "65537, 131072p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "3#  65537 --> 131072 = $aa3 "

aa4=$(sed -n "131073, 262144p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "4#  131073 --> 262144 =$aa4 "

aa5=$(sed -n "262145, 524288p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "5#  262145 --> 524288 = $aa5 "

aa6=$(sed -n "523289, 1048576p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "6#  523289 --> 1048576 =$aa6 "

aa7=$(sed -n "1048577, 2097152p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "7#  1048577 --> 2097152 = $aa7 "

aa8=$(sed -n "2097153, 4194304p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "8#  2097153 --> 4194304 =$aa8 "

aa9=$(sed -n "4194305, 8388608p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "9#  4194305 --> 8388608 =$aa9 "

aa10=$(sed -n "8388609, 16777216p" 182-all-mem-printf.txt | grep -c  "7f16846")
echo "10#  8388609 --> 16777216 =$aa10 "

aa11=$(sed -n '16777217, $p' 182-all-mem-printf.txt | grep -c  "7f16846")
echo "11#  16777217 --> END = $aa11"


if false; then
a=$(sed -n "1, 10099p" 182-all-mem-printf.txt)
grep -c  "7f16846" $a
fi
# wrong code:"grep: unrecognized option '-------------'
# Usage: grep [OPTION]... PATTERN [FILE]...
# Try 'grep --help' for more information  "

