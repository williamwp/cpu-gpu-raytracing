#ifndef CAMERAH
#define CAMERAH

#include "ray.h"

class camera {
    public:
        __device__ camera() {
            lower_left_corner = vec3(-2.0, -1.0, -1.0);
            horizontal = vec3(4.0, 0.0, 0.0);
            vertical = vec3(0.0, 2.0, 0.0);
            origin = vec3(0.0, 0.0, 0.0);
        }
	// the camera s at the negative side of z axis
	// we assume that the location of camera is called "lookat", the view of camera is called "lookfrom"

        __device__ ray get_ray(float u, float v) 
	{ 
	    return ray(origin, lower_left_corner + u*horizontal + v*vertical - origin); 
	}
	// without considering the field of view(fov)

        vec3 origin;
        vec3 lower_left_corner;
        vec3 horizontal;
        vec3 vertical;
};

#endif
